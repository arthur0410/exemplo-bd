import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


// Data access object
public class AlunoDao {

	private Connection conexao;

	public AlunoDao() {
		this.conexao = Conexao.getConnection();
	}

	public void inserir(Aluno aluno) {
		String query = "insert into aluno (serie, nome_turma, nome_aluno, nome_logradouro, numero_logradouro, cep) values (?, ?, ?, ?, ?, ?)";
		try {
			PreparedStatement statement = conexao.prepareStatement(query);

			statement.setInt(1, aluno.getSerie());
			statement.setString(2, aluno.getNomeTurma());
			statement.setString(3, aluno.getNomeAluno());
			statement.setString(4, aluno.getNomeLogradouro());
			statement.setInt(5, aluno.getNumeroLogradouro());
			statement.setInt(6, aluno.getCep());

			statement.execute();
			statement.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public boolean deletar(int numeroMatricula) {

		String query = "delete from aluno where numero_matricula = ?";

		try {
			PreparedStatement statement = conexao.prepareStatement(query);

			statement.setInt(1, numeroMatricula);

			boolean retorno = statement.execute();
			statement.close();

			return retorno;

		} catch (SQLException e) {
			// TODO: handle exception
			return false;
		}

	}

	public List<Aluno> consultar() {

		String query = "select * from aluno";

		List<Aluno> alunos = new ArrayList<Aluno>();

		try {
			PreparedStatement statement = conexao.prepareStatement(query);

			ResultSet resultado = statement.executeQuery();

			while(resultado.next()) {

				Aluno aluno = new Aluno();

				aluno.setNumeroMatricula(resultado.getInt(1));
				aluno.setSerie(resultado.getInt(2));
				aluno.setNomeTurma(resultado.getString(3));
				aluno.setNomeAluno(resultado.getString(4));
				aluno.setNomeLogradouro(resultado.getString(5));
				aluno.setNumeroLogradouro(resultado.getInt(6));
				aluno.setCep(resultado.getInt(7));

				alunos.add(aluno);
			}

			resultado.close();
			statement.close();

			return alunos;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public boolean atualizar(String nomeAntigo, String nomeNovo) {

		List<Aluno> alunos = new ArrayList<Aluno>();
		alunos = consultar(nomeAntigo);

		if(alunos.size() == 1) {

			String query = "update aluno set nome_aluno = ? where nome_aluno = ?";

			try {

				PreparedStatement statement = conexao.prepareStatement(query);
				statement.setString(1, nomeNovo);
				statement.setString(2, nomeAntigo);

				statement.execute();
				statement.close();
				return true;

			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		} else {
			return false;
		}

	}

	public List<Aluno> consultar(String nomeAluno) {

		String query = "select * from aluno where nome_aluno = ?";

		List<Aluno> alunos = new ArrayList<Aluno>();

		try {
			PreparedStatement statement = conexao.prepareStatement(query);

			statement.setString(1, nomeAluno);

			ResultSet resultado = statement.executeQuery();

			while(resultado.next()) {

				Aluno aluno = new Aluno();

				aluno.setNumeroMatricula(resultado.getInt(1));
				aluno.setSerie(resultado.getInt(2));
				aluno.setNomeTurma(resultado.getString(3));
				aluno.setNomeAluno(resultado.getString(4));
				aluno.setNomeLogradouro(resultado.getString(5));
				aluno.setNumeroLogradouro(resultado.getInt(6));
				aluno.setCep(resultado.getInt(7));

				alunos.add(aluno);
			}

			resultado.close();
			statement.close();

			return alunos;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

}
