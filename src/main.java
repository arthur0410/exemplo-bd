import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class main {

	public static void main(String[] args) throws SQLException {

//		Aluno aluno = new Aluno();
//		
//		aluno.setSerie(4);
//		aluno.setNomeTurma("A");
//		aluno.setNomeAluno("Guilherme Antonio 2");
//		aluno.setNomeLogradouro("Rua 4321");
//		aluno.setNumeroLogradouro(4321);
//		aluno.setCep(1234);
		
		AlunoDao alunoDao = new AlunoDao();
		//alunoDao.inserir(aluno);
		
//		if (alunoDao.deletar(1236)) {
//			System.out.println("Aluno deletado com sucesso!");
//		} else {
//			System.out.println("Erro ao deletar o aluno!");
//		}
		
		if(alunoDao.atualizar("Guilherme Antonio", "Guilherme Antonio 2")) {
			System.out.println("Nome do aluno atualizado com sucesso!");
		} else {
			System.out.println("Erro ao atualizar o nome do aluno!");
		}
		
		
//		List<Aluno> alunos = alunoDao.consultar();
//		
//		for(Aluno aluno : alunos) {
//			System.out.println(aluno.getNomeAluno());
//		}
//		
//		
//		System.out.println("Cliente inserido com sucesso!");
		
		
	}
}
